#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "add_constants.h"

void update_constants(uint64_t *const_0, uint64_t *const_1, uint64_t *const_2, uint64_t *const_3){
  uint64_t tmp;
  *const_0 = ((((*const_3 ^ *const_2) & 0x0000f00000000000) << 16) ^ 0xf000000000000000) ^ ((*const_0 & 0xf000000000000000) >> 16);
  *const_2 = *const_2 & 0xf000000000000000;
  *const_3 = *const_3 ^ 0x00000000f0000000;
  tmp = *const_0;
  *const_0 = *const_1;
  *const_1 = *const_2;
  *const_2 = *const_3;
  *const_3 = tmp;
}

void add_constants(uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3, uint64_t *const_0, uint64_t *const_1, uint64_t *const_2, uint64_t *const_3){
  update_constants(const_0, const_1, const_2, const_3);
  *registry_0 = *registry_0 ^ *const_0;
  *registry_1 = *registry_1 ^ *const_1;
  *registry_2 = *registry_2 ^ *const_2;
  *registry_3 = *registry_3 ^ *const_3;
}
