#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "add_round_tweakey.h"

void add_round_tweakey(uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3, uint64_t tk1_0, uint64_t tk1_1, uint64_t tk1_2, uint64_t tk1_3, uint64_t tk2_0, uint64_t tk2_1, uint64_t tk2_2, uint64_t tk2_3){
  *registry_0 = *registry_0 ^ ((tk1_0 ^ tk2_0) & 0xffffffff00000000);
  *registry_1 = *registry_1 ^ ((tk1_1 ^ tk2_1) & 0xffffffff00000000);
  *registry_2 = *registry_2 ^ ((tk1_2 ^ tk2_2) & 0xffffffff00000000);
  *registry_3 = *registry_3 ^ ((tk1_3 ^ tk2_3) & 0xffffffff00000000);
}
