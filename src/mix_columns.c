#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "mix_columns.h"

void mix_columns(uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3){
  mix_registry(registry_0);
  mix_registry(registry_1);
  mix_registry(registry_2);
  mix_registry(registry_3);
}

void mix_registry(uint64_t *registry){
  *registry = *registry ^ ((*registry & 0x00000000ffff0000) << 16);
  *registry = *registry ^ ((*registry & 0xffff000000000000) >> 32);
  *registry = *registry ^ ((*registry & 0x00000000ffff0000) >> 16);
  *registry = (*registry >> 16) ^ (*registry << 48);
}
