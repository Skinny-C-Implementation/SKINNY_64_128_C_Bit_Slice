#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "shift_rows.h"

const uint64_t shift_mask = 0x0000ffff00000000;

void shift_rows(uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3){
  registry_shift(registry_0);
  registry_shift(registry_1);
  registry_shift(registry_2);
  registry_shift(registry_3);
}

void registry_shift(uint64_t *registry){
  *registry = ((((*registry & shift_mask) >> 4) ^ ((*registry & shift_mask)) << 12) & shift_mask) ^ (*registry & (~shift_mask));
  *registry = ((((*registry & (shift_mask >> 16)) >> 8) ^ ((*registry & (shift_mask >> 16))) << 8) & (shift_mask >> 16)) ^ (*registry & (~(shift_mask >> 16)));
  *registry = ((((*registry & (shift_mask >> 32)) >> 12) ^ ((*registry & (shift_mask >> 32))) << 4) & (shift_mask >> 32)) ^ (*registry & (~(shift_mask >> 32)));
}
