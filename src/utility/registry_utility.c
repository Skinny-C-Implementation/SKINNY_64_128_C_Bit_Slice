#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "utility/registry_utility.h"

const uint64_t mask = 0x8000800080008000;

void packing(uint64_t plain_0, uint64_t plain_1, uint64_t plain_2, uint64_t plain_3, uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3){
  
  for (int i=0; i<4; i++){
    packing_per_registry_64(plain_0, plain_1, plain_2, plain_3, registry_0, i, 0);
    packing_per_registry_64(plain_0, plain_1, plain_2, plain_3, registry_1, i, 1);
    packing_per_registry_64(plain_0, plain_1, plain_2, plain_3, registry_2, i, 2);
    packing_per_registry_64(plain_0, plain_1, plain_2, plain_3, registry_3, i, 3);
  }
}

void unpacking(uint64_t *cipher_0, uint64_t *cipher_1, uint64_t *cipher_2, uint64_t *cipher_3, uint64_t registry_0, uint64_t registry_1, uint64_t registry_2, uint64_t registry_3){
  
  for (int i=0; i<4; i++){
    packing_per_registry_64(registry_0, registry_1, registry_2, registry_3, cipher_0, i, 0);
    packing_per_registry_64(registry_0, registry_1, registry_2, registry_3, cipher_1, i, 1);
    packing_per_registry_64(registry_0, registry_1, registry_2, registry_3, cipher_2, i, 2);
    packing_per_registry_64(registry_0, registry_1, registry_2, registry_3, cipher_3, i, 3);
  }
}


void packing_per_registry_64(uint64_t plain_0, uint64_t plain_1, uint64_t plain_2, uint64_t plain_3, uint64_t *registry, int round, int index_no){
  *registry = ((plain_0 & ((mask >> index_no) >> round*4)) << index_no) ^ *registry;
  *registry = (((plain_1 & ((mask >> index_no) >> round*4)) << index_no) >> 1) ^ *registry;
  *registry = (((plain_2 & ((mask >> index_no) >> round*4)) << index_no) >> 2) ^ *registry;
  *registry = (((plain_3 & ((mask >> index_no) >> round*4)) << index_no) >> 3 ) ^ *registry;
}

void registry_permutation(uint64_t *registry, uint64_t permutation_registry){
  uint64_t tmp = 0x0;
  uint8_t nibble;
  
  for(int i=0; i<16; i++){
    nibble = (permutation_registry & (0xf000000000000000 >> 4*i)) >> (60 - (4*i));
    if (nibble > i){
      tmp = tmp ^ ((*registry & (0xf000000000000000 >> 4*nibble)) << ((nibble -i)*4));
    }
    else{
      tmp = tmp ^ ((*registry & (0xf000000000000000 >> 4*nibble)) >> ((i - nibble)*4));
      }
     
  }

  *registry = tmp;
}
