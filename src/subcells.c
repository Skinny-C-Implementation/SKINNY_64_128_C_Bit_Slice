#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "subcells.h"

void subcells(uint64_t *x0, uint64_t *x1, uint64_t *x2, uint64_t *x3){
  *x0 = *x0 ^ (~(*x3 | *x2));
  *x3 = *x3 ^ (~(*x2 | *x1));
  *x2 = *x2 ^ (~(*x1 | *x0));
  *x1 = *x1 ^ (~(*x0 | *x3));
 
  uint64_t tmp = *x0;
  *x0 = *x1;
  *x1 = *x2;
  *x2 = *x3;
  *x3 = tmp;
}
