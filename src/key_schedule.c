#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "key_schedule.h"
#include "utility/registry_utility.h"

const uint64_t permutation_registry = 0x9f8daecb01234567;

void key_schedule(uint64_t *tk1_0, uint64_t *tk1_1, uint64_t *tk1_2, uint64_t *tk1_3, uint64_t *tk2_0, uint64_t *tk2_1, uint64_t *tk2_2, uint64_t *tk2_3){
  key_permutation(tk1_0, tk1_1, tk1_2, tk1_3);
  key_permutation(tk2_0, tk2_1, tk2_2, tk2_3);
  key_lfsr(tk2_0, tk2_1, tk2_2, tk2_3);
}

void key_permutation(uint64_t *tk_0, uint64_t *tk_1, uint64_t *tk_2, uint64_t *tk_3){
  registry_permutation(tk_0, permutation_registry);
  registry_permutation(tk_1, permutation_registry);
  registry_permutation(tk_2, permutation_registry);
  registry_permutation(tk_3, permutation_registry);
}

void key_lfsr(uint64_t *tk_0, uint64_t *tk_1, uint64_t *tk_2, uint64_t *tk_3){
  uint64_t tmp = *tk_0;
  *tk_0 = (*tk_0 & 0x00000000ffffffff) ^ (*tk_1 & 0xffffffff00000000);
  *tk_1 = (*tk_1 & 0x00000000ffffffff) ^ (*tk_2 & 0xffffffff00000000);
  *tk_2 = (*tk_2 & 0x00000000ffffffff) ^ (*tk_3 & 0xffffffff00000000);
  *tk_3 = (*tk_3 & 0x00000000ffffffff) ^ ((tmp ^ *tk_0) & 0xffffffff00000000);
}
