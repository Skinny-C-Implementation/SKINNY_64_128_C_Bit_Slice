#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>

#include "utility/registry_utility.h"
#include "subcells.h"
#include "add_constants.h"
#include "add_round_tweakey.h"
#include "shift_rows.h"
#include "mix_columns.h"
#include "key_schedule.h"
#include "utility/timing.h"

int main(int argc, char *argv[]){

  /* VARIABLE DECLARATION */

  uint64_t plain_0;
  uint64_t plain_1;
  uint64_t plain_2;
  uint64_t plain_3;
  uint64_t tk1;
  uint64_t tk2;
  uint64_t registry_0 = 0x0;
  uint64_t registry_1 = 0x0;
  uint64_t registry_2 = 0x0;
  uint64_t registry_3 = 0x0;
  uint64_t tk1_0 = 0x0;
  uint64_t tk1_1 = 0x0;
  uint64_t tk1_2 = 0x0;
  uint64_t tk1_3 = 0x0;
  uint64_t tk2_0 = 0x0;
  uint64_t tk2_1 = 0x0;
  uint64_t tk2_2 = 0x0;
  uint64_t tk2_3 = 0x0;
  uint64_t c = 0x0;
  uint64_t const_0 = 0x0;
  uint64_t const_1 = 0x0;
  uint64_t const_2 = 0x0;
  uint64_t const_3 = 0x0;
  uint64_t cipher_0 = 0x0;
  uint64_t cipher_1 = 0x0;
  uint64_t cipher_2 = 0x0;
  uint64_t cipher_3 = 0x0;
  
  uint64_t timer;

  /* VARIABLE DECLARATION */

  /* VARIABLE SCAN */

  printf("Plain Text 1:\n");
  scanf("%" SCNx64, &plain_0);
  printf("Plain Text 2:\n");
  scanf("%" SCNx64, &plain_1);
  printf("Plain Text 3:\n");
  scanf("%" SCNx64, &plain_2);
  printf("Plain Text 4:\n");
  scanf("%" SCNx64, &plain_3);
  printf("Tweakey 1st part:\n");
  scanf("%" SCNx64, &tk1);
  printf("Tweakey 2nd part:\n");
  scanf("%" SCNx64, &tk2);
  
  /* VARIABLE SCAN */

  timer = start_rdtsc();
  
  /* PACKING */

  packing(plain_0, plain_1, plain_2, plain_3, &registry_0, &registry_1, &registry_2, &registry_3);
  packing(tk1, tk1, tk1, tk1, &tk1_0, &tk1_1, &tk1_2, &tk1_3);
  packing(tk2, tk2, tk2, tk2, &tk2_0, &tk2_1, &tk2_2, &tk2_3);
  packing(c, c, c, c, &const_0, &const_1, &const_2, &const_3);

  /* PACKING */

  /* CIPHER ROUNDS */
  
  for(int i=0; i<36; i++){
    subcells(&registry_3, &registry_2, &registry_1, &registry_0);
    add_constants(&registry_0, &registry_1, &registry_2, &registry_3, &const_0, &const_1, &const_2, &const_3);
    add_round_tweakey(&registry_0, &registry_1, &registry_2, &registry_3, tk1_0, tk1_1, tk1_2, tk1_3, tk2_0, tk2_1, tk2_2, tk2_3);
    shift_rows(&registry_0, &registry_1, &registry_2, &registry_3);
    mix_columns(&registry_0, &registry_1, &registry_2, &registry_3);
    key_schedule(&tk1_0, &tk1_1, &tk1_2, &tk1_3, &tk2_0, &tk2_1, &tk2_2, &tk2_3);
  }

  /* CIPHER ROUNDS */

  /* UNPACKING */
  
  unpacking(&cipher_0, &cipher_1, &cipher_2, &cipher_3, registry_0, registry_1, registry_2, registry_3);

  /* UNPACKING */

  timer = end_rdtsc()- timer;
  
  /* CIPHER TEXT PRINTING */
  
  printf("Cipher text 1: %"PRIx64 "\n", cipher_0);
  printf("Cipher text 2: %"PRIx64 "\n", cipher_1);
  printf("Cipher text 3: %"PRIx64 "\n", cipher_2);
  printf("Cipher text 4: %"PRIx64 "\n", cipher_3);
  printf("Cycles/byte: %f\n", (((double)timer)/32));

  /* CIPHER TEXT PRINTING */
}
