#pragma once

void update_constants(uint64_t *const_0, uint64_t *const_1, uint64_t *const_2, uint64_t *const_3);
void add_constants(uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3, uint64_t *const_0, uint64_t *const_1, uint64_t *const_2, uint64_t *const_3);
