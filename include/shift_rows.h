#pragma once

void shift_rows(uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3);

void registry_shift(uint64_t *registry);
