#pragma once

void packing(uint64_t plain_0, uint64_t plain_1, uint64_t plain_2, uint64_t plain_3, uint64_t *registry_0, uint64_t *registry_1, uint64_t *registry_2, uint64_t *registry_3);
void unpacking(uint64_t *cipher_0, uint64_t *cipher_1, uint64_t *cipher_2, uint64_t *cipher_3, uint64_t registry_0, uint64_t registry_1, uint64_t registry_2, uint64_t registry_3);
void packing_per_registry_64(uint64_t plain_0, uint64_t plain_1, uint64_t plain_2, uint64_t plain_3, uint64_t *registry, int round, int index_no);
void registry_permutation(uint64_t *registry, uint64_t permutation_registry);
