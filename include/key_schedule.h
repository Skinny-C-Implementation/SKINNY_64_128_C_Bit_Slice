#pragma once

void key_schedule(uint64_t *tk1_0, uint64_t *tk1_1, uint64_t *tk1_2, uint64_t *tk1_3, uint64_t *tk2_0, uint64_t *tk2_1, uint64_t *tk2_2, uint64_t *tk2_3);
void key_permutation(uint64_t *tk_0, uint64_t *tk_1, uint64_t *tk_2, uint64_t *tk_3);
void key_lfsr(uint64_t *tk_0, uint64_t *tk_1, uint64_t *tk_2, uint64_t *tk_3);
