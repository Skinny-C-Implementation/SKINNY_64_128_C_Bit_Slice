SRCDIR=src
SRCUTILDIR=$(SRCDIR)/utility
LIBDIR=lib
BINDIR=bin
INCLUDEDIR=include
CC=gcc
AR=ar
CFLAGS=-Wall -pedantic -g -std=gnu99 -O3 -I$(INCLUDEDIR) -I$(HOME)/local/include -lm
LDFLAGS=-L$(LIBDIR) -lutility -lcipher
EXEC=Skinny_64_128_Bit_Slice
CREATE=mkdir

const :
	@ if ! [ -d $(BINDIR) ]; then \
		echo "Création du dossier $(BINDIR)"; \
		$(CREATE) $(BINDIR); \
	fi
	@ if ! [ -d $(LIBDIR) ]; then \
		echo "Création du dossier $(LIBDIR)"; \
		 $(CREATE) $(LIBDIR); \
	fi

all : const $(BINDIR)/$(EXEC)

$(SRCDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

$(LIBDIR)/libutility.a : $(SRCUTILDIR)/registry_utility.o $(SRCUTILDIR)/timing.o 
	$(AR) -r $@ $^

$(LIBDIR)/libcipher.a : $(SRCDIR)/subcells.o $(SRCDIR)/add_constants.o $(SRCDIR)/add_round_tweakey.o $(SRCDIR)/shift_rows.o $(SRCDIR)/mix_columns.o $(SRCDIR)/key_schedule.o
	$(AR) -r $@ $^

$(BINDIR)/$(EXEC) : $(SRCDIR)/main.o $(LIBDIR)/libutility.a $(LIBDIR)/libcipher.a 
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)

$(SRCDIR)/%.o : $(SRCDIR)/%.c
	$(CC) -o $@ -c $< $(CFLAGS)

clean :
	rm -rf ./$(BINDIR)/*
	rm -rf ./$(LIBDIR)/*.a
	rm -rf ./$(SRCDIR)/*.o
	rm -rf ./$(SRCUTILDIR)/*.o
	rm -rf ./$(SRCDIR)/*.c~
	rm -rf ./$(SRCUTILDIR)/*.c~
	rm -rf ./$(INCLUDEDIR)/*.h~
	rm -rf ./$(INCLUDEDIR)/utility/*.h~
