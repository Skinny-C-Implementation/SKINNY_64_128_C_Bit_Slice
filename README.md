# Skinny-64-128 Bit Sliced version

## Notes
[Reference paper](https://eprint.iacr.org/2016/660.pdf)

The plain text in this version is 64 bits long, and the tweakey is 128 bits long. The tweakey is divided into 2 64 bits part.

The Skinny lightweight cipher is composed of 5 steps + 1 Tweakey Schedule:
- Sub Cells
- Add Constants
- Add Round Tweakey
- Shift Rows
- Mix Columns

This version takes 4 plaintexts and 1 key. The four plaintexts and the key are respectively packed into four registries in this way:
-R0 contains all the MSB of the first nibble of each plaintexts, then all the MSB of the second nibble, etc.
-R1 contains all the second bits of a nibble in the same order.
-R2 contains all the third bits of a nibble in the same order.
-R3 contains all the LSB in the same order.

This packing allows some processing to be simplier and faster.
For example, the subcells is done by a simple XOR between all the registries.

This version contains only the encryption.

The makefile contains the O3 option. If you want to erase it or use O2 you need to modify the following line in the makefile:
```bash
CFLAGS=-Wall -pedantic -g -std=gnu99 -O3 -I$(INCLUDEDIR) -I$(HOME)/local/include -lm
```

## Use:

To compile the program on a Unix system, open a Terminal in the directory containing this repository's content.

```bash
make clean
make all
```

Note: If some directories are missing, you may need to redo the commands one more time.

Then, to use the program, do:

```bash
./bin/Skinny_64_128_Bit_Slice
```

The program will ask for 4 plain texts and for the key separated into two parts.
Once the plaintexts and the key are sent, the encryption begin and the result is printed.

With:

```bash
Plain 1: 2c81c9975446f930
Plain 2: d12d7cd55446f930
Plain 3: c12d7cd57446f930
Plain 4: 046be0baecc1f283
Key 1st: d57aafd67af44834
Key 2nd: 28d61a94c501d2fe
```

You should obtain:
```bash
Cipher text 1: 94cea83e21b1df3
Cipher text 2: 1ad69d00910e9c57
Cipher text 3: 80688bb568f4c4d
Cipher text 4: 90ea84689db6e2c8
```